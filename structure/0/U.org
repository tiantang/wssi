/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM Extend Project: Open Source CFD        |
|  \\    /   O peration     | Version:  1.6-ext                               |
|   \\  /    A nd           | Web:      www.extend-project.de                 |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       volVectorField;
    location    "0";
    object      U;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

dimensions      [0 1 0 0 0 0 0];

internalField   uniform (0 0 0);

boundaryField
{
  bottom
    {
        type            fixedValue;
        value uniform (0 0 0);
    }
  
    left
    {
        type            tractionDisplacement;
        traction uniform (0 0 0);
        pressure      uniform  0;
        value uniform (0 0 0);	
    }

    right
    {
        type            tractionDisplacement;
        traction uniform (0 0 0);
        pressure      uniform  0;
        value uniform (0 0 0);	
    }

    front
    {
        type            tractionDisplacement;
        traction uniform (0 0 0);
        pressure      uniform  0;
        value uniform (0 0 0);	
    }

    back
    {
        type            wavePressureTractionDisplacement;
        traction uniform (0 0 0);
        pressure      uniform  0;
        value uniform (0 0 0);	
    }

    top
    {
        type            tractionDisplacement;
        traction uniform (0 0 0);
        pressure      uniform  0;
        value uniform (0 0 0);	
       // type            timeVaryingMappedFixedValue;
        //setAverage  off;
    }
}

// ************************************************************************* //
