#!/bin/bash

source_start=$1;
source_end=$2;

for((i=${source_start}; i<=${source_end}; i++))
do

cp ../../../../3Dwaves/surfaces/$i/waveBottomPressure/scalarField/pd $i/p

sed -i '1cFoamFile{version     2.0;format      ascii;class       scalarAverageField; object      values;} 0.0' $i/p

if [[ i -ne ${source_end} ]];
	then
		temp=`echo "$i+0.5" | bc -l`;
   cp ../../../../3Dwaves/surfaces/$temp/waveBottomPressure/scalarField/pd $temp/p

sed -i '1cFoamFile{version     2.0;format      ascii;class       scalarAverageField; object      values;} 0.0' $temp/p
fi

done
