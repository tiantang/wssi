#!/bin/bash

source_start=$1;
source_end=$2;

for((i=${source_start}; i<=${source_end}; i++))
do

cp ../../../../structure/surfaces/$i/waveBottomPressure/symmTensorField/sigma $i/sigma_S

sed -i '1cFoamFile{version     2.0;format      ascii;class       symmTensorAverageField; object      values;} (0.0 0.0 0.0 0.0 0.0 0.0)' $i/sigma_S

if [[ i -ne ${source_end} ]];
	then
		temp=`echo "$i+0.5" | bc -l`;
   cp ../../../../structure/surfaces/$temp/waveBottomPressure/symmTensorField/sigma $temp/sigma_S

    sed -i '1cFoamFile{version     2.0;format      ascii;class       symmTensorAverageField; object      values;} (0.0 0.0 0.0 0.0 0.0 0.0)' $temp/sigma_S
fi

done
